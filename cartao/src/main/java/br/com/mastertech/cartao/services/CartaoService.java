package br.com.mastertech.cartao.services;

import br.com.mastertech.cartao.clients.ClienteClient;
import br.com.mastertech.cartao.models.Cartao;
import br.com.mastertech.cartao.models.Cliente;
import br.com.mastertech.cartao.models.dtos.CartaoDTOEntrada;
import br.com.mastertech.cartao.models.dtos.CartaoDTOSaida;
import br.com.mastertech.cartao.models.dtos.CartaoDTOSaidaAtivoOculto;
import br.com.mastertech.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Iterable<Cartao> listarTodos(){
        return cartaoRepository.findAll();
    }

    public CartaoDTOSaida buscarPorId(int id){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if (cartaoOptional.isPresent()){
            CartaoDTOSaida cartaoDTOSaida = new CartaoDTOSaida(
                    cartaoOptional.get().getId(), cartaoOptional.get().getNumero(),
                    cartaoOptional.get().getIdCliente(), cartaoOptional.get().isAtivo());
            return cartaoDTOSaida;
        } else {
            throw new RuntimeException("O cartão com id: "+ id + " não foi encontrado.");
        }
    }

    public CartaoDTOSaidaAtivoOculto buscarPorNumero(String numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if (cartaoOptional.isPresent()){
            CartaoDTOSaidaAtivoOculto cartaoDTOSaidaAtivoOculto = new CartaoDTOSaidaAtivoOculto(
                    cartaoOptional.get().getId(), cartaoOptional.get().getNumero(), cartaoOptional.get().getIdCliente());
            return cartaoDTOSaidaAtivoOculto;
        } else {
            throw new RuntimeException("O cartão nº "+ numero +" não foi encontrado!");
        }
    }

    public CartaoDTOSaida salvar(CartaoDTOEntrada cartaoDTOEntrada){
        //Optional<Cliente> clienteOptional = clienteRepository.findById(cartaoDTOEntrada.getClienteId());
        Optional<Cliente> clienteOptional = clienteClient.buscarPorId(cartaoDTOEntrada.getClienteId());

        if (clienteOptional.isPresent()){
            Cliente cliente = new Cliente();
            Cartao cartao = new Cartao(cartaoDTOEntrada.getNumero(), false, clienteOptional.get().getId());
            Cartao cartaoSalvo = cartaoRepository.save(cartao);
            CartaoDTOSaida cartaoDTOSaida = new CartaoDTOSaida(
                    cartaoSalvo.getId(), cartaoSalvo.getNumero(), cartaoSalvo.getIdCliente(), cartaoSalvo.isAtivo());
            return cartaoDTOSaida;
        }
        else
        {
            throw new RuntimeException("O cliente "+ cartaoDTOEntrada.getClienteId()+" não foi encontrado!");
        }
    }

    public CartaoDTOSaida alteraStatusAtivo(String numero, CartaoDTOSaida cartaoDTOSaida){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if (cartaoOptional.isPresent()){
            cartaoOptional.get().setAtivo(cartaoDTOSaida.isAtivo());

            Cartao cartao = new Cartao(cartaoOptional.get().getNumero(),cartaoOptional.get().isAtivo(), cartaoOptional.get().getIdCliente());
            cartao.setId(cartaoOptional.get().getId());

            Cartao cartaoSalvo = cartaoRepository.save(cartao);

            cartaoDTOSaida = new CartaoDTOSaida(
                    cartaoSalvo.getId(), cartaoSalvo.getNumero(), cartaoSalvo.getIdCliente(), cartaoSalvo.isAtivo());
            return cartaoDTOSaida;
        } else {
            throw new RuntimeException("O cartão não foi encontrado!");
        }
    }

}
