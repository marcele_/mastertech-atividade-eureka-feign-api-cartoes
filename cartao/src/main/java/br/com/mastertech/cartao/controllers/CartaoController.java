package br.com.mastertech.cartao.controllers;

import br.com.mastertech.cartao.models.Cartao;
import br.com.mastertech.cartao.models.dtos.CartaoDTOEntrada;
import br.com.mastertech.cartao.models.dtos.CartaoDTOSaida;
import br.com.mastertech.cartao.models.dtos.CartaoDTOSaidaAtivoOculto;
import br.com.mastertech.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @GetMapping("/cartoes")
    public Iterable<Cartao> listar(){
        try {
            return cartaoService.listarTodos();
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/cartao/{id}")
    public CartaoDTOSaida buscarPorId(@Valid @PathVariable(name = "id") int id) {
        try {
            return cartaoService.buscarPorId(id);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/cartao/n/{numero}")
    public CartaoDTOSaidaAtivoOculto buscarPorNumero(@Valid @PathVariable(name = "numero") String numero){
        try{
            return cartaoService.buscarPorNumero(numero);
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/cartao")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CartaoDTOSaida> cadastrar(@RequestBody @Valid CartaoDTOEntrada cartaoDTOEntrada){
        try{
            return ResponseEntity.status(201).body(cartaoService.salvar(cartaoDTOEntrada));
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PatchMapping("/cartao/{numero}")
    public CartaoDTOSaida alteraStatusAtivo(@Valid @PathVariable(name = "numero" ) String numero, @RequestBody CartaoDTOSaida cartaoDTOSaida){
        try{
            return cartaoService.alteraStatusAtivo(numero, cartaoDTOSaida);
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
