package br.com.mastertech.pagamento.services;

import br.com.mastertech.pagamento.clients.CartaoClient;
import br.com.mastertech.pagamento.models.Cartao;
import br.com.mastertech.pagamento.models.Pagamento;
import br.com.mastertech.pagamento.models.dtos.PagamentoDTOEntrada;
import br.com.mastertech.pagamento.models.dtos.PagamentoDTOSaida;
import br.com.mastertech.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Iterable<PagamentoDTOSaida> buscarPorIdCartao(int idCartao){
        Optional<Cartao> cartaoOptional = cartaoClient.buscarPorId(idCartao);

        if (cartaoOptional.isPresent()){
            Cartao cartao = new Cartao();
            cartao = cartaoOptional.get();

            //List<Pagamento> listaLancamentos = pagamentoRepository.findAllByCartao(cartao);
            List<Pagamento> listaLancamentos = pagamentoRepository.findAllByIdCartao(cartao.getId());
            List<PagamentoDTOSaida> listaPagamentoDTOSaida = new ArrayList<>();

            for (Pagamento pagamentoObjeto: listaLancamentos) {
                PagamentoDTOSaida pagamentoDTOSaida = new PagamentoDTOSaida(
                        pagamentoObjeto.getId(), pagamentoObjeto.getIdCartao(),
                        pagamentoObjeto.getDescricao(), pagamentoObjeto.getValor());

                listaPagamentoDTOSaida.add(pagamentoDTOSaida);
            }

            return listaPagamentoDTOSaida;
        } else {
            throw new RuntimeException("O cartão não foi encontrado!");
        }
    }

    public PagamentoDTOSaida salvar(PagamentoDTOEntrada pagamentoDTOEntrada){
        Optional<Cartao> cartaoOptional = cartaoClient.buscarPorId(pagamentoDTOEntrada.getCartao_id());

        if (cartaoOptional.isPresent()){
            Pagamento pagamento = new Pagamento(pagamentoDTOEntrada.getDescricao(), pagamentoDTOEntrada.getValor(),
                    cartaoOptional.get().getId());
            Pagamento pagamentoRealizado = pagamentoRepository.save(pagamento);

            PagamentoDTOSaida pagamentoDTOSaida = new PagamentoDTOSaida(
                    pagamentoRealizado.getId(), pagamentoRealizado.getIdCartao(),
                    pagamentoRealizado.getDescricao(), pagamentoRealizado.getValor());
            return pagamentoDTOSaida;
        }
        else
        {
            throw new RuntimeException("O cartão não foi encontrado e o pagamento não foi realizado!");
        }
    }

}
